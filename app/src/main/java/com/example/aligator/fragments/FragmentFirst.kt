package com.example.aligator.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.aligator.R

class FragmentFirst : Fragment(R.layout.fragment_first){
    private lateinit var editTextNote : EditText
    private lateinit var buttonAdd : Button
    private lateinit var textView : TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextNote = view.findViewById(R.id.editTextNote)
        buttonAdd = view.findViewById(R.id.button3)
        textView = view.findViewById(R.id.textView)


        val sharedPreferences = requireActivity().getSharedPreferences("NOTES", Context.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTE","")
        textView.text = text

        buttonAdd.setOnClickListener {
            val order1 = editTextNote.text.toString()
            val order2 = textView.text.toString()

            val res = order1 + "\n" + order2

            textView.text = res

            sharedPreferences.edit()
                .putString("Note",res)
                .apply()

        }




    }
}




